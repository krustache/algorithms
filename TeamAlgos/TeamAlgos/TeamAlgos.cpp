#include <iostream>
#include <string>

#pragma region SumOfDigitsRecursive
/* write a recursive function that returns the sum of the digits of a given integer.
    input: 12345
    ouput: 15
*/
int sumOfDigits(int number, int sum);

int main1()
{
    int a = 123456789;
    int result = 0;

    result = a % 10;

    int result2 = sumOfDigits(a, 0);
    std::cout << result2;
    return 0;
}

int sumOfDigits(int number, int sum)
{
    if (number < 1)
    {
        return sum;
    }
    else
    {
        sum += number % 10;
        sumOfDigits(number / 10, sum);
    }
}
#pragma endregion

#pragma region MinimumLengthWordInString

/*

Given a string S (that can contain multiple words), you need to find the word which has minimum length.

Note: If multiple words are of the same length, then answer will be the first minimum length word in the string. Words are separated by single space only.

Input: "abc de ghihfh a uvw h j"
Output: a

*/

std::string FindMinimumLength(std::string input)
{
    std::string result = input;
    std::string currentShortestWord = "";

    for(int i = 0; i < input.length(); i++)
    {

        if (input[i] != ' ')
        {
            currentShortestWord += input[i];
        }
        
        if (input[i] == ' ' || i == input.length() - 1)
        {
            if (currentShortestWord.length() < result.length())
            {
                result = currentShortestWord;
                std::cout << "current shortest word = " << result << std::endl;

            }
            currentShortestWord = "";
        }
	}

    return result;
}

int main2()
{
    std::string a = "aasdfasdf defs ghihfh abb uvw bafdsa ja";

    std::string shortestWord = FindMinimumLength(a);

    std::cout << shortestWord;

    return 0;
}


#pragma endregion





